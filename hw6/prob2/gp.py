#!/usr/bin/python

import os, sys
import numpy as np
import matplotlib.pyplot as plt
import math
from matplotlib.backends.backend_pdf import PdfPages

sigma = 1

# mean function
def m(x):
    return math.sin(x*math.pi/100)+(x/100.0)

# Covariance function
def K(x1, x2):
    return 1/16.0 * math.e ** ( - ((x1-x2)/sigma)**2/2)

def GP(n):
    xvec = range(1, n+1)
    mu = [m(x) for x in xvec]
    Sigma = [[K(x1,x2) for x1 in xvec] \
             for x2 in xvec]
    yvec = np.random.multivariate_normal(mu,Sigma,1).T
    return xvec, yvec

def condGP(n):
    xvec_seen = [40, 120]
    xvec = range(1,n+1); 
    xvec.pop(39); xvec.pop(118)

    f = np.matrix([0, 1]).T
    k_starn = np.matrix([[K(x1,x2) for x1 in xvec_seen] for x2 in xvec])
    k_nstar = k_starn.T
    k_nn = np.matrix([[K(x1,x2) for x1 in xvec_seen] for x2 in xvec_seen])
    k_starstar = np.matrix([[K(x1,x2) for x1 in xvec] for x2 in xvec])

    mu_orig = np.matrix([m(x) for x in xvec]).T
    mu = k_starn * k_nn.getI() * f - mu_orig
    mu = np.squeeze(np.asarray(mu.T))
    Sigma = k_starstar - k_starn * k_nn.getI() * k_nstar

    yvec = list(np.random.multivariate_normal(mu, Sigma,1).T)

    xvec.insert(39, 40); xvec.insert(119, 120); 
    yvec.insert(39, 0);  yvec.insert(119, 1)

    return xvec, yvec


def draw_dotconnect_with_sigma(s=40, process=GP, n=100, mean=True):

    # part 1 -- 6 plot with different sample from the distribution. Draw
    # graph using those samples.
    global sigma
    sigma = s
    ax = plt.subplot(111)
    for i in range(6):
        pt = process(n)
        plt.plot(pt[0], pt[1], label="i=%d"%(i+1));
    
    if mean:
        plt.plot([m(x) for x in range(n)], label="m(x)");

    plt.ylabel("f(x)")
    plt.xlabel("x")

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.15,
                     box.width, box.height * 0.85])
    
    # Put a legend below current axis
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1),
               fancybox=True, shadow=True, ncol=4)


def prob_abc():
    pp = PdfPages('Figure_abc.pdf')
    for i in [40, 10, 1]:
        plt.title('sigma=%2d' % i)
        draw_dotconnect_with_sigma(i, process=GP, n=100, mean=True)
        plt.savefig(pp, format='pdf')
        plt.clf()
        #plt.show()
    pp.close()

def prob_def():    
    pp = PdfPages('Figure_def.pdf')
    for s in [40, 10, 1]:
        plt.title('sigma=%2d' % s)
        draw_dotconnect_with_sigma(s, process=condGP, n=120, mean=False)
        plt.savefig(pp, format='pdf')
        plt.clf()
        #plt.show()
    pp.close()

prob_abc()
prob_def()
