#!/usr/bin/python

import os, sys, math
import struct
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

a = 10
precision = 10000

def randomint(n):
    return struct.unpack("<%dL"%n, os.urandom(4*n))

def sample_and_test(cnt=1000):
    t = randomint(2*cnt)
    YLIM, THETALIM = a * precision, 90*precision
    ylist, thetalist = t[:cnt], t[cnt:]
    ylist = [y%YLIM/float(precision) for y in ylist] # bottom most point
    thetalist = [(theta%THETALIM) / float(precision) for theta in thetalist]

    c = 0
    val_pi = [0 for _ in xrange(cnt)]
    i = 0
    for y,theta in zip(ylist, thetalist):
        sinetheta = math.sin(math.radians(theta))
        ymoda = y % a
        if ymoda+a*sinetheta >= a:
            c += 1
        if c != 0:
            val_pi[i]  = 2*i/float(c)
        i += 1

    return val_pi

CNT=10000
pt = sample_and_test(cnt=CNT)
print pt[-1]
plt.plot(range(CNT), pt)
plt.xlabel("n")
plt.ylabel("Estimated value of pi")
pp = PdfPages('Val_pi.pdf')
plt.savefig(pp, format='pdf')
pp.close()
